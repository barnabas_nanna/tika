<?php

class Psr3Watchdog extends \Psr\Log\AbstractLogger {

  static $level_map = array(
    \Psr\Log\LogLevel::EMERGENCY => WATCHDOG_EMERGENCY,
    \Psr\Log\LogLevel::ALERT => WATCHDOG_ALERT,
    \Psr\Log\LogLevel::CRITICAL => WATCHDOG_CRITICAL,
    \Psr\Log\LogLevel::ERROR => WATCHDOG_ERROR,
    \Psr\Log\LogLevel::WARNING => WATCHDOG_WARNING,
    \Psr\Log\LogLevel::NOTICE => WATCHDOG_NOTICE,
    \Psr\Log\LogLevel::INFO => WATCHDOG_INFO,
    \Psr\Log\LogLevel::DEBUG => WATCHDOG_DEBUG,
  );

  private $type = 'PSR-3';

  /**
   * Sets the type of watchdog entries created by this Psr3Watchdog instance.
   * If not set, 'PSR-3' is used.
   *
   * @param string $type
   *   The category to which this message belongs. Can be any string, but
   *   the general practice is to use the name of the module calling watchdog().
   */
  public function setType($type) {
    $this->type = $type;
  }

  /**
   * Logs with an arbitrary level.
   *
   * @param mixed $level
   * @param string $message
   * @param array $context
   * @return null
   */
  public function log($level, $message, array $context = array()) {
    if (variable_get('psr3_watchdog_min_severity', WATCHDOG_WARNING) >= $level) {
      $variables = array();
      foreach ($context as $key => $value) {
        if (strpos($message, '{' . $key . '}') !== FALSE) {
          $variables['@' . $key] = $value;
          $message = str_replace('{' . $key . '}', '@' . $key, $message);
        }
      }

      watchdog(
        $this->type,
        $message,
        $variables + $context,
        Psr3Watchdog::$level_map[$level]
      );
    }
  }
}
