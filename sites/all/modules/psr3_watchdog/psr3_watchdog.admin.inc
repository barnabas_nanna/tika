<?php

function psr3_watchdog_admin_settings($form, &$form_state) {

  composer_manager_register_autoloader();

  $form['psr3_watchdog_min_severity'] = array(
    '#type' => 'select',
    '#title' => t('Severity'),
    '#options' => array_flip(Psr3Watchdog::$level_map),
    '#default_value' => variable_get('psr3_watchdog_min_severity', WATCHDOG_WARNING),
    '#description' => t('Set the minimum severity level for log messages to appear in the log.'),
  );

  return $form;
}
